package me.textcalculator;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

/**
 * Runs an application's activity.
 *
 * @author Vladislav
 * @version 1.0
 * @since 03/17/2021
 */
public class SecondActivity extends AppCompatActivity {
    private long firstArgument = 0, secondArgument = 0;

    /**
     * Creates the application's activity.
     *
     * @param savedInstanceState previous saved data states.
     */
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        Bundle arguments = getIntent().getExtras();
        firstArgument = arguments.getInt("FIRST_ARGUMENT");
        secondArgument = arguments.getInt("SECOND_ARGUMENT");

        ((TextView) findViewById(R.id.firstArgumentValue)).setText((firstArgument < 0) ? "(" + firstArgument + ")" : String.valueOf(firstArgument));
        ((TextView) findViewById(R.id.secondArgumentValue)).setText((secondArgument < 0) ? "(" + secondArgument + ")" : String.valueOf(secondArgument));
        ((TextView) findViewById(R.id.result)).setText(String.valueOf(firstArgument + secondArgument));
    }

    /**
     * Restores data states from {@code savedInstanceState}.
     *
     * @param savedInstanceState data states for restoring.
     */
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        firstArgument = savedInstanceState.getLong("FIRST_ARGUMENT_STATE");
        secondArgument = savedInstanceState.getLong("SECOND_ARGUMENT_STATE");
    }

    /**
     * Saves data states to {@code outState}.
     *
     * @param outState data states for saving.
     */
    protected void onSaveInstanceState(Bundle outState) {
        outState.putLong("FIRST_ARGUMENT_STATE", firstArgument);
        outState.putLong("SECOND_ARGUMENT_STATE", secondArgument);
        super.onSaveInstanceState(outState);
    }
}