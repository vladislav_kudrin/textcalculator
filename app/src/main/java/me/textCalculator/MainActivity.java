package me.textcalculator;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.content.Intent;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Runs an application's activity.
 *
 * @author Vladislav
 * @version 1.0
 * @since 03/17/2021
 */
public class MainActivity extends AppCompatActivity {
    private Intent secondActivity;
    private String firstArgumentInput = "", secondArgumentInput = "";

    /**
     * Creates the application's activity.
     *
     * @param savedInstanceState previous saved data states.
     */
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        secondActivity = new Intent(this, SecondActivity.class);
    }

    /**
     * Restores data states from {@code savedInstanceState}.
     *
     * @param savedInstanceState data states for restoring.
     */
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        firstArgumentInput = savedInstanceState.getString("FIRST_ARGUMENT_INPUT_STATE");
        secondArgumentInput = savedInstanceState.getString("SECOND_ARGUMENT_INPUT_STATE");
    }

    /**
     * Saves data states to {@code outState}.
     *
     * @param outState data states for saving.
     */
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString("FIRST_ARGUMENT_INPUT_STATE", firstArgumentInput);
        outState.putString("SECOND_ARGUMENT_INPUT_STATE", secondArgumentInput);
        super.onSaveInstanceState(outState);
    }

    /**
     * Checks arguments' values, if correct puts them into {@code secondActivity} and starts {@code secondActivity}.
     *
     * @param view user interface components.
     */
    public void onClick(View view) {
        firstArgumentInput = ((TextView) findViewById(R.id.firstArgumentInput)).getText().toString();
        secondArgumentInput = ((TextView) findViewById(R.id.secondArgumentInput)).getText().toString();

        try {
            int firstArgument = Integer.parseInt(firstArgumentInput);
            int secondArgument = Integer.parseInt(secondArgumentInput);

            secondActivity.putExtra("FIRST_ARGUMENT", firstArgument);
            secondActivity.putExtra("SECOND_ARGUMENT", secondArgument);
            startActivity(secondActivity);
        } catch (Exception exception) {
            if (firstArgumentInput.isEmpty() || secondArgumentInput.isEmpty()) Toast.makeText(this, "Введите оба слагаемых!", Toast.LENGTH_SHORT).show();
            else Toast.makeText(this, "Значения не могут быть менее -2 147 483 648 или более 2 147 483 647!", Toast.LENGTH_SHORT).show();
        }
    }
}